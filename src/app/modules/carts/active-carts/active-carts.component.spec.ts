import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveCartsComponent } from './active-carts.component';

describe('ActiveCartsComponent', () => {
  let component: ActiveCartsComponent;
  let fixture: ComponentFixture<ActiveCartsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActiveCartsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveCartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
