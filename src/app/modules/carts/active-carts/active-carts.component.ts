import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { StoreService } from 'src/app/service/store.service';

@Component({
  selector: 'app-active-carts',
  templateUrl: './active-carts.component.html',
  styleUrls: ['./active-carts.component.scss']
})
export class ActiveCartsComponent implements OnInit {
  activeCartLog: any[] = [];

  @Input() search : any; 
  constructor(private router: Router, private store: StoreService, private dataStore: DataService) {

  }

  ngOnInit(): void {
    this.store.data.subscribe(res => {

      this.activeCartLog = res;
      console.log("parent",this.search);
    })
    let dataId = localStorage.getItem('id')
  
    if (dataId) {
      this.activeCartLog.map((item, i) => {
        if (item.id == dataId) {
          this.activeCartLog.splice(i, 1);
         
        }
      })
    }
    localStorage.removeItem('id')

  }

  editRow(event: any) {
    this.dataStore.updatedData(event);
    this.router.navigate(['/place-order'])
  }
  addCarts() {
    this.router.navigate(['/create'])
  }
}
