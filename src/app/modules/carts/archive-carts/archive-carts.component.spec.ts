import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchiveCartsComponent } from './archive-carts.component';

describe('ArchiveCartsComponent', () => {
  let component: ArchiveCartsComponent;
  let fixture: ComponentFixture<ArchiveCartsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArchiveCartsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchiveCartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
