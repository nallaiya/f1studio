import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-carts-details',
  templateUrl: './carts-details.component.html',
  styleUrls: ['./carts-details.component.scss']
})
export class CartsDetailsComponent implements OnInit {
  isClicked:boolean=false;
  isActive:boolean=false
  activeCartLog:any[]=[];
  term:any;

    constructor(private router:Router) {
     
     }
  
    ngOnInit(): void {
  
    }
    carts(){
      this.isClicked=false;
    }
    favorites(){
      this.isClicked=true;
     

    }
    activeCart(){
      this.isActive=false;
    }
    ArchiveCart(){
      this.isActive=true;
     
    }
    onSearch(){
      this.term;
  
    }

}
