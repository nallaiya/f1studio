import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';
import { CartsDetailsComponent } from './carts-details.component';
import { SharedModule } from '../shared/shared.module';
import { ActiveCartsComponent } from './active-carts/active-carts.component';
import { CreateNewOrderComponent } from './create-new-order/create-new-order.component';
import { ArchiveCartsComponent } from './archive-carts/archive-carts.component';
import { ConfigureComponent } from './configure/configure.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { PlaceOrderComponent } from './place-order/place-order.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
const routes: Routes = [
  {path:'cart',component:CartsDetailsComponent},
  {path:'active',component:ActiveCartsComponent},
  {path:'archive',component:ArchiveCartsComponent},
  {path:'create',component:CreateNewOrderComponent},
  {path:'configure',component:ConfigureComponent},
  {path:'order-details',component:OrderDetailsComponent},
  {path:'place-order',component:PlaceOrderComponent},
  {
    path: '',
    redirectTo: 'cart',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'cart',
    pathMatch: 'full'
  }
]


@NgModule({
  declarations: [
    CartsDetailsComponent,
    ActiveCartsComponent,
    CreateNewOrderComponent,
    ArchiveCartsComponent,
    ConfigureComponent,
    OrderDetailsComponent,
    PlaceOrderComponent,
    
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    FormsModule,
    RouterModule.forChild(routes)
  ]
})
export class CartsModule { }
