import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-configure',
  templateUrl: './configure.component.html',
  styleUrls: ['./configure.component.scss']
})
export class ConfigureComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  configure(){
   this.router.navigate(['/order-details'])
  }
  navigation(){
    this.router.navigate(['/create']);
  }
}
