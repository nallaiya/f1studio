import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-new-order',
  templateUrl: './create-new-order.component.html',
  styleUrls: ['./create-new-order.component.scss']
})
export class CreateNewOrderComponent implements OnInit {
orderList:any[]=[];
  constructor(private router:Router) { 
    this.orderList=[
    {'id':1,title:'Configure your door from scratch',sub_text:'Residential, Commerical, Entry doors'},
    {'id':2,title:'Parts / Openers',sub_text:'Order Parts and Openers'},
    {'id':3,title:'Direct Item Entry',sub_text:'Order Parts / Openers using a Product ID'},
    {'id':4,title:'Choose from favourites',sub_text:'Re-order door from your favourites '},
    ]
  }

  ngOnInit(): void {
  }
  createOrder(event:any){
    if(event.id === 1){
      this.router.navigate(['/configure'])
    }
  }
  navigation(){
    this.router.navigate(['/cart']);
  }
}
