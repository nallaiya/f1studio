import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { StoreService } from 'src/app/service/store.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {
  formValid:boolean=false;
  additionalOptions:any=[];
  count=25;
  progresStyle:any;
  notify:any;

  
  itemForm = new FormGroup({
    assembly_type: new FormControl('',[Validators.required]),
    measure_size_a: new FormControl('',[Validators.required]),
    measure_size_b: new FormControl('',[Validators.required]),
    measure_height_a: new FormControl('',[Validators.required]),
    measure_height_b: new FormControl('',[Validators.required]),
    windcode: new FormControl('',[Validators.required]),
    designe: new FormControl('',[Validators.required]),
    color: new FormControl('',[Validators.required]),
    glass_type: new FormControl('',[Validators.required]),
    Section_glazed: new FormControl('',[Validators.required]),
    framing: new FormControl('',[Validators.required]),
    spring: new FormControl('',[Validators.required]),
    track_size:new FormControl('',[Validators.required]),
    track_mount:new FormControl('',[Validators.required]),
    track_lift:new FormControl('',[Validators.required]),
    track_radius:new FormControl('',[Validators.required]),
    lock:new FormControl('',[Validators.required]),
    no_lock:new FormControl('',[Validators.required]),
    packaging:new FormControl('',[Validators.required]),
    struts:new FormControl('',[Validators.required]),
    other:new FormControl(this.additionalOptions)
  });
  storedData:any[]=[]
  constructor(private router:Router,private store:StoreService,private dataStore:DataService) { 
    this.additionalOptions=[
      {id:1,opt:'Extra Strut(s)'},
      {id:2,opt:'Spade Strap Hinge(s)'},
      {id:3,opt:'14 GA Quiet Hinge'},
      {id:4,opt:'Less Bottom Astragal'},
      {id:5,opt:'Gold Bar Gurantee'},
      {id:6,opt:'Medallion Hardware Upgrade'},
      {id:7,opt:'Light Seal Kit'},
      {id:8,opt:'Slide Lock Mounted at 54”'},
      {id:9,opt:'2 Spear Lift Handles'},
    ]
  }
  placeOrder(){
  
    if(this.itemForm.valid){
      let id=Math.floor(Math.random() * 1001 + 5 - 45);
      this.itemForm.value['id']=id;
      this.store.data.subscribe(res=>{
        this.storedData=res;
      })
      this.storedData.push(this.itemForm.value)
      this.store.updatedDataSelection(this.storedData);
     
      this.dataStore.updatedData(this.itemForm.value);
      
      this.router.navigate(['/place-order']);
    }
   
  

  }  
  
  otherOption(event:any){
    this.itemForm.value.other.push({'id':event.id,'opt':event.opt})
   
   
  }
  navigation(){
    this.router.navigate(['/configure']);
  }
  ngOnInit(): void {
    this.dataStore.data.subscribe(res=>{
 
      
      if(res!=null){
        this.itemForm.patchValue({
          assembly_type: res.assembly_type,
          measure_size_a: res.measure_size_a ,
          measure_size_b:res.measure_size_b ,
          measure_height_a:res.measure_height_a ,
          measure_height_b:res.measure_height_b ,
          windcode:res.windcode ,
          designe:res.designe ,
          color:res.color ,
          glass_type:res.glass_type ,
          Section_glazed:res.Section_glazed ,
          framing: res.framing,
          spring: res.spring,
          track_size:res.track_size,
          track_mount:res.track_mount,
          track_lift:res.track_lift,
          track_radius:res.track_radius,
          lock:res.lock,
          no_lock:res.no_lock,
          packaging:res.packaging,
          struts:res.struts,
          other:res.other
        })

        for(let i=0;i<this.additionalOptions.length;i++){
          console.log(this.additionalOptions[i].id,res.other[i].id)
          if(this.additionalOptions[i].id!=res.other[i].id){
            this.additionalOptions[i].type=false
          }else{
            this.additionalOptions[i].type=true
          }
        }
        
      }
    })
  }
  ngDoCheck(){
    if(this.itemForm.valid){
      this.formValid=true;
    }else{
      this.formValid=false;
    }
    
  }
}
