import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { StoreService } from 'src/app/service/store.service';

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.scss']
})
export class PlaceOrderComponent implements OnInit {
  title: any;
  jobName: any;
  id:any;
  constructor(private router: Router,private store: StoreService, private dataStore: DataService) { }

  ngOnInit(): void {
    this.dataStore.data.subscribe(res => {
      this.title = res.assembly_type + res.track_size,
        this.jobName = res.glass_type,
        this.id = res.id
    })

  }
  edit() {
    localStorage.setItem('id',this.id)
    this.router.navigate(['/order-details'])
  }
  addMore() {
    this.router.navigate(['/create']);
  }
  orderPlace() {
    this.router.navigate(['/cart']);
  }
  delete() {
    localStorage.setItem('id',this.id)
    this.router.navigate(['/cart']);
  }
  navigation(){
    this.router.navigate(['/order-details']);
  }
}
