import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FavComponent } from './fav/fav.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path:'favs',component:FavComponent},
 
]

@NgModule({
  declarations: [
    FavComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class FavoritesModule { }
