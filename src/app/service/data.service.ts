import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {


  private dataPass = new BehaviorSubject<any>(null);
  data = this.dataPass.asObservable();
    constructor() { }
  
    updatedData(data: any){
      this.dataPass.next(data);
    }
}
