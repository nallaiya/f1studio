import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class StoreService {
log = [
    {
      'id': 1,
      'assembly_type': 'Offcut Door',
      'measure_size_a': '8 ft',
      'measure_size_b': '7 ft',
      'measure_height_a': '8 ft',
      'measure_height_b': '7 ft',
      'windcode': 'WindCode W1',
      'designe': 'CC',
      'color': 'Dark',
      'glass_type': 'Solid (No Windows)',
      'Section_glazed': 'Section(s) Glazed',
      'framing': 'Arch 1 Design',
      'spring': 'Galvanized Torsion',
      'track_size': '2” Flag & Jamb Brackets Loose',
      'track_mount': 'Bracket',
      'track_lift': 'Track Lift',
      'track_radius': '12” Radius',
      'lock': 'Inside Slide Lock (#2)',
      'no_lock': 'no_lock',
      'packaging': '[x] Distributor',
      'struts': '5 Extra Struts'
    },
    {
      'id': 1,
      'assembly_type': 'Full Door',
      'measure_size_a': '8 ft',
      'measure_size_b': '7 ft',
      'measure_height_a': '8 ft',
      'measure_height_b': '7 ft',
      'windcode': 'WindCode W1',
      'designe': 'CC',
      'color': 'Dark',
      'glass_type': 'Solid (No Windows)',
      'Section_glazed': 'Section(s) Glazed',
      'framing': 'Arch 1 Design',
      'spring': 'Galvanized Torsion',
      'track_size': '2” Flag & Jamb Brackets Loose',
      'track_mount': 'Bracket',
      'track_lift': 'Track Lift',
      'track_radius': '12” Radius',
      'lock': 'Inside Slide Lock (#2)',
      'no_lock': 'no_lock',
      'packaging': '[x] Distributor',
      'struts': '5 Extra Struts'
    }

  ]
 
  
private dataSource = new BehaviorSubject<any>(this.log);
data = this.dataSource.asObservable();
  constructor() { 
    console.log(typeof(this.log));
  }

  updatedDataSelection(datas: any){
    this.dataSource.next(datas);
   
  }
}
